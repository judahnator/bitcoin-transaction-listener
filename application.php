<?php

require 'vendor/autoload.php';

use judahnator\BitcoinTransactionListener\Commands\ListenToBlocks;
use judahnator\BitcoinTransactionListener\Commands\ListenToTransactions;
use judahnator\BitcoinTransactionListener\Commands\TransactionSummary;
use Symfony\Component\Console\Application;

$application = new Application();

$application->addCommands([
    new ListenToTransactions(),
    new TransactionSummary(),
    new ListenToBlocks()
]);

try {
    $application->run();
} catch (Exception $e) {
    echo $e->getMessage();
}
