<?php

namespace judahnator\BitcoinTransactionListener\Commands;


use Carbon\Carbon;
use judahnator\BitcoinTransactionEventLoop\Exceptions\TransactionStreamException;
use judahnator\BitcoinTransactionEventLoop\Services\TransactionsService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TransactionSummary extends Command
{

    private $transactions = [];
    private $terminalSize;
    private $startTime;
    private $totalTransactions = 0;

    public function __construct(?string $name = null)
    {
        parent::__construct($name);
        $this->terminalSize = [
            'height' => exec('tput lines'),
            'width' => exec('tput cols')
        ];
        $this->startTime = Carbon::now();;
    }

    public function configure()
    {
        $this
            ->setName('transactions:summary')
            ->setDescription('Creates a basic summary of the transactions in the past little bit.')
            ->addOption('show-transactions', 't', InputArgument::OPTIONAL, 'Displays incoming transactions in real time', true)
            ->addOption('update-frequency', 'u', InputArgument::OPTIONAL, 'How often to update the display (in seconds)', 5);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $TransactionsService = new TransactionsService();

        $TransactionsService->beforeEvent(function() {
            $this->transactions = array_filter(
                $this->transactions,
                function($key) {
                    return Carbon::createFromTimestamp($key) >= Carbon::now()->subMinutes(10);
                },
                ARRAY_FILTER_USE_KEY
            );
        });

        $TransactionsService->duringEvent(function($transaction) {
            $time = time();
            if (!array_key_exists($time, $this->transactions)) {
                $this->transactions[$time] = [];
            }
            $this->transactions[time()][] = $transaction;
        });

        if ($input->getOption('show-transactions')) {
            $TransactionsService->afterEvent(function() use ($output) {
                $output->write('.');
            });
        }

        $TransactionsService->afterEvent(function() {
            $this->totalTransactions++;
        });

        $TransactionsService->periodicCallback(
            function() use ($output) {
                $outputMessages = [];

                $minutesSinceStart = $this->startTime->diffInMinutes();
                if ($minutesSinceStart >= 15) {
                    $outputMessages[] = '15 minute average: '.self::minuteAverage($this->transactions, 15);
                }

                if ($minutesSinceStart >= 5) {
                    $outputMessages[] = '5  minute average: '.self::minuteAverage($this->transactions, 5);
                }

                if ($minutesSinceStart >= 1) {
                    $outputMessages[] = '1  minute average: '.self::minuteAverage($this->transactions, 1);
                }

                if ($minutesSinceStart > 0) {
                    array_unshift($outputMessages, 'Transaction volume in BTC:');
                } else {
                    $outputMessages[] = 'Collecting data...';
                }

                $metadataMessage = 'Processed '. $this->totalTransactions.' transaction so far';
                if ($this->startTime->diffInMinutes() > 0) {
                    $metadataMessage .= ' averaging ~'. round($this->totalTransactions / $this->startTime->diffInSeconds() * 60, 1) .' per minute';
                }
                $outputMessages[] = $metadataMessage;

                $output->writeln(str_repeat(PHP_EOL, $this->terminalSize['height'] - count($outputMessages)));
                $output->writeln($outputMessages);
            },
            $input->getOption('update-frequency')
        );

        try {
            $TransactionsService->run();
        }catch (TransactionStreamException $e) {
            $output->writeln("<error>{$e->getMessage()}</error>");
        }
    }

    private static function minuteAverage(array $transactions, int $minutes) {
        $transactionsInTimeframe = array_filter(
            $transactions,
            function ($key) use ($minutes) {
                return Carbon::createFromTimestamp($key) >= Carbon::now()->subMinutes($minutes);
            },
            ARRAY_FILTER_USE_KEY
        );

        $flattenedTransactions = [];
        foreach ($transactionsInTimeframe as $time => $timeFrameTransactions) {
            $flattenedTransactions = array_merge($flattenedTransactions, $timeFrameTransactions);
        }

        return round(
            transaction_array_sum($flattenedTransactions) / count($flattenedTransactions),
            6
        );
    }

}