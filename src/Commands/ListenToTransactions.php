<?php

namespace judahnator\BitcoinTransactionListener\Commands;


use Carbon\Carbon;
use judahnator\BitcoinTransactionEventLoop\Exceptions\TransactionStreamException;
use judahnator\BitcoinTransactionEventLoop\Services\TransactionsService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class ListenToTransactions extends Command
{

    private $startTime;

    public function __construct(?string $name = null)
    {
        parent::__construct($name);
        $this->startTime = Carbon::now();
    }

    protected function configure()
    {
        $this
            ->setName('transactions:listen')
            ->setDescription('Connects to the blockchain.info websocket and displays transaction info')
            ->setHelp('This command displays current bitcoin transactions')
            ->addOption('duration', null, InputOption::VALUE_OPTIONAL, 'The number of minutes this program will run', 5);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $TransactionService = new TransactionsService();

        $TransactionService->beforeEvent(function() use ($output, $input, $TransactionService) {
            if ($this->startTime->diffInMinutes() > (int)$input->getOption('duration')) {
                $TransactionService->halt();
            }
        });

        $TransactionService->duringEvent(function($transaction) use ($output) {

            $txSum = transaction_sum($transaction);

            // Makes everything line up. Don't question it.
            $beforeAmountBuffer = 5 - strlen((string)round($txSum));
            $beforeAmountPadding = $beforeAmountBuffer > 0 ? str_repeat(" ", $beforeAmountBuffer) : "";
            $beforeHashPadding = str_repeat(" ", 13 - strlen((string)$txSum) + strlen((string)round($txSum)));

            $output->writeln("[ ".Carbon::now()->format("H:i:s")." ]".$beforeAmountPadding.$txSum.$beforeHashPadding.$transaction->x->hash);

        });

        try {
            $TransactionService->run();
        } catch (TransactionStreamException $e) {
            $output->writeln("<error>{$e->getMessage()}</error>");
        }
    }

}