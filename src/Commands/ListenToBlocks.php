<?php

namespace judahnator\BitcoinTransactionListener\Commands;


use Carbon\Carbon;
use judahnator\BitcoinTransactionEventLoop\Services\BlocksService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ListenToBlocks extends Command
{

    private $latestBlock, $terminalSize;

    public function __construct(?string $name = null)
    {
        parent::__construct($name);

        $this->terminalSize = [
            'height' => exec('tput lines'),
            'width' => exec('tput cols')
        ];

    }

    public function configure()
    {
        $this
            ->setName('blocks:listen')
            ->setDescription('Connects to the blockchain.info websocket and displays block information')
            ->setHelp('This command outputs blocks as they are created')
            ->addOption('with-notification', 'notify', InputOption::VALUE_OPTIONAL, 'Flags that this script should send system notifications');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $loop = new BlocksService();

        $loop->sendMessage('{"op":"ping_block"}');

        $loop->duringEvent(function($block) use ($output) {
            $this->latestBlock = $block;

            $eta = Carbon::now()
                ->setTimezone('America/Chicago')
                ->subSeconds((int)file_get_contents('https://blockchain.info/q/eta'))
                ->format('g:i:s a');

            $blockData = $this->latestBlock->x;

            $output->write(str_repeat(PHP_EOL, $this->terminalSize['height'] - 4));
            $output->writeln([
                'The last block was #'.$this->latestBlock->x->height,
                'Details can be viewed at: https://blockchain.info/block-index/'.$blockData->blockIndex.'/'.$blockData->hash,
                'This block contains '.count($blockData->txIndexes).' transactions summing aproximately '.round($blockData->totalBTCSent / 100000000, 2).' BTC',
                'The next block should arrive at apriximitely '.$eta
            ]);
        });

        if ($input->getOption('with-notification')) {
            $loop->afterEvent(function() use ($input) {
                $this->sendNotification($input->getOption('with-notification'));
            });
        }

        $loop->run();
    }

    private function sendNotification(string $driver) {
        $title = "Block #{$this->latestBlock->x->height}";
        $body = count($this->latestBlock->x->txIndexes)." transactions, ".round($this->latestBlock->x->totalBTCSent / 100000000, 2)."BTC sent.";

        switch ($driver) {

            case "gnome":
                $command = "notify-send \"{$title}\" \"{$body}\"";
                break;

            case "mac":
                $command = "osascript -e 'display notification \"{$body}\" with title \"{$title}\"''";
                break;

            default:
                // unknown driver
                return;

        }

        $process = new Process($command);
        $process->run();
        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
    }

}