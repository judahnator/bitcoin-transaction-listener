<?php

if (!function_exists('transaction_sum')) {
    function transaction_sum($transaction) {
        return array_sum(
            array_map(
                function ($txOutput) {
                    return $txOutput->value / 100000000;
                },
                $transaction->x->out
            )
        );
    }
}

if (!function_exists('transaction_array_sum')) {
    function transaction_array_sum($transactions) {
        return array_sum(
            array_map(
                'transaction_sum',
                $transactions
            )
        );
    }
}